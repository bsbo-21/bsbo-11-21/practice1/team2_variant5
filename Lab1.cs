using System;
using System.Collections.Generic;
using System.Reflection;

public class User
{
    public string Name { get; set; }
    public int Age { get; set; }
    public string City { get; set; }
    public string Password { get; set; }

    public Friends Friends { get; set; }

    public User()
    {
        Friends = new Friends();
    }

    // Проверка на правильность пароля
    public bool CheckPassword(string password)
    {
        return Password == password;
    }
}

public class Friends
{
    public List<User> FriendsList { get; set; }

    public Friends()
    {
        FriendsList = new List<User>();
    }

    public void AddFriend(User friend)
    {
        FriendsList.Add(friend);
        Console.WriteLine("Класс Friends: Добавлен друг " + friend.Name);
    }

    public void RemoveFriend(User friend)
    {
        FriendsList.Remove(friend);
        Console.WriteLine("Класс Friends: Удален друг " + friend.Name);
    }
}

public class Server
{
    public List<User> Users { get; set; }
    public List<Message> Messages { get; set; }
    public List<Application> Applications { get; set; }
    public Search Search { get; set; } // Добавлено поле Search

    public Server()
    {
        Users = new List<User>();
        Messages = new List<Message>();
        Applications = new List<Application>();
        Search = new Search(this); // Создание экземпляра Search, передаем текущий сервер в качестве параметра
    }

    public void GetAge(int age)
    {
        // Реализация метода GetAge
        Console.WriteLine("Класс Server, метод GetAge: Получен возраст " + age);
    }

    public void GetCity(string city)
    {
        // Реализация метода GetCity
        Console.WriteLine("Класс Server, метод GetCity: Получен город " + city);
    }

    public void GetName(string name)
    {
        // Реализация метода GetName
        Console.WriteLine("Класс Server, метод GetName: Получено имя " + name);
    }

    public User CreateUser(string name, int age, string city, string password)
    {
        User user = new User
        {
            Name = name,
            Age = age,
            City = city,
            Password = password
        };

        Users.Add(user);
        Console.WriteLine("Класс Server, метод CreateUser: Пользователь: " + user.Name + ", " + user.Age + ", " + user.City + ", " + " зарегистрирован");

        return user;
    }

    public void CancelUser(User user)
    {
        Users.Remove(user);
        Console.WriteLine("Класс Server, метод CancelUser: Пользователь: " + user.Name + ", " + user.Age + ", " + user.City + ", " + " аннулирован");
    }

    public void AddFriend(User user, User friend)
    {
        user.Friends.AddFriend(friend);
        Console.WriteLine("Класс Server, метод AddFriend: Пользователь " + friend.Name + " добавлен в список друзей пользователя " + user.Name);
    }

    public void RemoveFriend(User user, User friend)
    {
        user.Friends.RemoveFriend(friend);
        Console.WriteLine("Класс Server, метод RemoveFriend: Пользователь " + friend.Name + " удален из списка друзей пользователя " + user.Name);
    }

    public void SendMessage(User sender, string description)
    {
        // Реализация метода SendMessage
        Console.WriteLine("Класс Server, метод SendMessage: Отправлено сообщение от " + sender.Name + ": " + description);

        // Создаем новый объект Message
        Message message = new Message();
        message.Description = description;
        message.Sender = sender;

        // Добавляем сообщение в список сообщений
        Messages.Add(message);
    }

    public void AddApplication(Application application)
    {
        Applications.Add(application);
        Console.WriteLine("Класс Server, метод AddApplication: Заявка добавлена");
    }

    public void ConnectUser(User user)
    {
        // Проверка на правильность пароля
        Console.WriteLine("Класс Server, метод ConnectUser: Проверка пароля пользователя " + user.Name);
        if (user.CheckPassword(user.Password))
        {
            Console.WriteLine("Класс Server, метод ConnectUser: Пользователь " + user.Name + " подключен");
        }
        else
        {
            Console.WriteLine("Класс Server, метод ConnectUser: Неверный пароль. Подключение не выполнено");
        }
    }

    public void DisconnectUser(User user)
    {
        Console.WriteLine("Класс Server, метод DisconnectUser: Пользователь " + user.Name + " отключен");
    }
}

public class Message
{
    public string Description { get; set; }
    public User Sender { get; set; }
}

public class Search
{
    public Server Server { get; set; } // Добавлено поле Server

    public Search(Server server) // Добавлен конструктор
    {
        Server = server;
    }

    public void SendApplication(string notification)
    {
        // Реализация метода SendApplication
        Console.WriteLine("Класс Search, метод SendApplication: Отправлена заявка: " + notification);
        
        Application application = new Application();
        application.Name = "Заявка 1";
        Server.AddApplication(application);
    }
}

public class Application
{
    public string Name { get; set; }

    public void AddToFriends(string name)
    {
        // Реализация метода AddToFriends
        Console.WriteLine("Класс Search, метод AddToFriends: Пользователь " + name + " добавлен в список друзей");
    }
}

public class Program
{
    static void Main(string[] args)
    {
        // Создание экземпляра класса Server
        Server server = new Server();

        // Пример 1: Регистрация пользователей
        User user1 = server.CreateUser("Петя", 20, "Москва", "Пароль1");
        User user2 = server.CreateUser("Женя", 21, "Санкт-Петербург", "Пароль2");
        User user3 = server.CreateUser("Вася", 22, "Воронеж", "Пароль3");
        Console.WriteLine();

        // Пример 2: Аннулирование пользователя
        server.CancelUser(user1);
        Console.WriteLine();

        // Пример 3: Подключение пользователя с проверкой пароля
        server.ConnectUser(user2);
        server.ConnectUser(user3);
        Console.WriteLine();

        // Пример 4: Отключение пользователя
        server.DisconnectUser(user2);
        Console.WriteLine();

        // Пример 5: Поиск друзей
        server.GetAge(21);
        server.GetCity("Санкт-Петербург");
        server.GetName("Женя");
        Search search = new Search(server); // Создание объекта Search с передачей сервера в конструктор
        search.SendApplication("Новая заявка"); // Вызов метода SendApplication из объекта Search
        server.ConnectUser(user2);
        server.AddFriend(user3, user2);
        Console.WriteLine();

        // Пример 6: Отправка сообщения
        server.SendMessage(user3, "Привет!");

        Console.WriteLine();

        // Вывод списков
        Console.WriteLine("Список пользователей:");
        foreach (User user in server.Users)
        {
            Console.WriteLine("Имя: " + user.Name + ", Возраст: " + user.Age + ", Город: " + user.City);
        }
        Console.WriteLine("\nСписок сообщений:");
        foreach (Message msg in server.Messages)
        {
            Console.WriteLine("Описание: " + msg.Description);
        }

        Console.WriteLine("\nСписок заявок:");
        foreach (Application app in server.Applications)
        {
            Console.WriteLine("Имя: " + app.Name);
        }

        Console.ReadLine();
    }
}
