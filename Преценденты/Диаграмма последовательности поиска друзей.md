```plantuml
@startuml
title Процесс поиска друзей

actor Пользователь
participant Server
participant Search
participant Application
participant Friends

Пользователь -> Server : Запросить поиск друзей
activate Server
Server -> Search: GetAge(Age)
activate Search
Search --> Пользователь: Результат поиска по возрасту 
Server -> Search: GetCity(City)
Search --> Пользователь: Результат поиска по городу
Server -> Search: GetName(Name)
Search --> Пользователь: Результат поиска по Имени
Search -> Application: SendApplication(notification)
deactivate Search
activate Application
Application -> Server: Запись данных на сервер
Application -> Friends: AddToFriends(name)
deactivate Application
activate Friends
Friends -> Server: Запись данных на сервер
deactivate Friends
Server -> Пользователь: Отправить уведомление о новом друге
deactivate Server



@enduml
```
