```plantuml
@startuml

class User {
  +Name: string
  +Age: int
  +City: string
  +Password: string
  +Friends: Friends
  --
  +CheckPassword(password: string): bool
}

class Friends {
  +FriendsList: List<User>
  --
  +AddFriend(friend: User)
  +RemoveFriend(friend: User)
}

class Server {
  +Users: List<User>
  +Messages: List<Message>
  +Applications: List<Application>
  --
  +GetAge(age: int)
  +GetCity(city: string)
  +GetName(name: string)
  +CreateUser(name: string, age: int, city: string, password: string): User
  +CancelUser(user: User)
  +AddFriend(user: User, friend: User)
  +RemoveFriend(user: User, friend: User)
  +SendMessage(sender: User, description: string)
  +AddApplication(application: Application)
  +ConnectUser(user: User)
  +DisconnectUser(user: User)
}

class Message {
  +Description: string
  +Sender: User
}

class Search {
  +Users: List<User>
  +Notification: string
  +Server: Server
  --
  +SendApplication(notification: string)
}

class Application {
  +Name: string
  --
  +AddToFriends(name: string)
}

User "1" *-- "1" Friends
Server "1" *-- "*" User
Server "1" *-- "*" Message
Server "1" *-- "*" Application
Server "1" *-- "1" Search
Search "1" o-- "1" Server
Search "1" *-- "*" User
Server "1" o-- "*" User
Server "1" o-- "*" Message
Server "1" o-- "*" Application

@enduml
```
